<?php
$connection = mysqli_connect('localhost', 'root', '') or die('Could not connect the database : Username or password incorrect');
$db_selected = mysqli_select_db($connection, 'view1');
$data_collector = array();
$data_collector1 = array();
$que = 'SELECT DISTINCT Value FROM definition WHERE Type="Categories"';
$que1 = 'SELECT DISTINCT Value FROM definition WHERE Type="ConnectTo"';
$result1 = mysqli_query($connection, $que);
$res1 = mysqli_query($connection, $que);
$result2 = mysqli_query($connection, $que1);
$res2 = mysqli_query($connection, $que1);
// $options="";
while ($row2 = mysqli_fetch_array($result1)) {
    // $data_collector=$row2[0];
    $new = array_push($data_collector, $row2[0]);
}
while ($row1 = mysqli_fetch_array($result2)) {
    // $data_collector=$row2[0];
    $new1 = array_push($data_collector1, $row1[0]);
}
// echo $data_collector[0];
// echo $new;

mysqli_close($connection);
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DataEntry</title>
    <link href="style.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"
        type="text/css" /> -->
        
        <!-- <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" /> -->
    
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous"> -->
    <link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
    <!-- <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.min.css" /> -->
        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="js/jquery.min.js"></script>        
    <script src="js/jquery.datetimepicker.js"></script>
    <script src="date.js"></script>





</head>

<body>
    <div class="header">
        <h1>
            <center>Task Management Schedular</center>
            
        </h1>
    </div>
    <form method="POST" name="POST">
        <div class=" container">
            <div class="wrapper">
                <button type="button" class="btn btn-info" id="Schedule">Schedule Date :</button>
                <!-- <input type="date" id="Cal" name="date"> -->
                <div  class="input-group date" data-date-format="dd-mm-yyyy">
                <input id="datepicker" class="aj" name="date" placeholder="Date">
                <span class="input-group-addon" id="CalenderIcon"><i id="calicn" class="fas fa-calendar-week"></i></span>
                </div>

                <hr class="solid">

            </div>
         
        </div>
        <div>
   

        <table class=" table" id="Table" width="100%">
        <col style="width:2.5% !important">
	<col style="width: 2.5% !important;">
	<col style="width:2.5% !important">
    <col style="width:80% !important">
    <col style="width:2.5% !important">
    <col style="width:2.5% !important">
    <col style="width: 2.5% !important;">
    <col style="width:2.5% !important">
    <col style="width:auto">

            <thead id="hd">
                <th id="small" onclick="sortTable()">Priority</th>
                <th>Categories</th>
                <th>Connet To</th>
                <th>Task</th>
                <th>Option 1</th>
                <th>Option 2</th>
                <th>Option 3</th>
                <th>Status</th>
                <th>Action</th>

            </thead>
            <tbody class="TableBod" id="tablebody">
                <tr class="all work">
                    <td class="Priority" data-label="Priority">
                        <span class="Priority_border" id="PriorityTxt1">1</span>
                        <input type="hidden" class="Hidden_Pri" id="hidden_field_id1" name="id0" value="1" />
                        <input type="hidden" id="Uni_Id1" name="Uni0" value="1" />

                    </td>
                    <td class="Categories" data-label="Categories">
                        <div class="dropdown">
                            <select class="selectpicker" name="Categories0" data-style="btn-success" id="dropdownMenuButton0">
                                <option hidden>Categories</option>
                                <?php while ($r1 = mysqli_fetch_array($res1)) :; ?>
                                    <option value="<?php echo $r1[0]; ?>"><?php echo $r1[0]; ?></option>
                                <?php endwhile; ?>
                                <!-- <option value="all">All</option>
                                <option value="work">Work</option>
                                <option value="personal">Personal</option>
                                <option value="prospect">Prospect</option> -->

                            </select>
                        </div>

                    </td>

                    <td class="ConnetTo" data-label="Connet To">

                        <div class="dropdown">
                            <select class="selectpicker" name="ConnetTo0" data-style="btn-success" id="Client0">
                                <option hidden>Client/Prospect</option>
                                <?php while ($r2 = mysqli_fetch_array($res2)) :; ?>
                                    <option value="<?php echo $r2[0]; ?>"><?php echo $r2[0]; ?></option>
                                <?php endwhile; ?>
                                <!-- <option value="Jai">Jai</option>
                                <option value="Kamal">Kamal</option>
                                <option value="Ajay">Ajay</option>
                                <option value="Gopi">Gopi</option> -->
                            </select>
                        </div>


                    </td>
                    <td class="Task" data-label="Task">
                        <div class="form-group">

                            <textarea class="form-control" rows="1" name="note0" id="comment0"></textarea>
                        </div>
                    </td>

                    <td class="Option1" data-label="Option 1">
                        <div class="OptionDetails">
                            <div class="dropdown">
                                <select class="selectpicker options" id="Option10" name="Option10" data-style="btn-success">
                                <option hidden>Op1</option>
                                    <option value="Meet">Meet</option>
                                    <option value="Call">Call</option>
                                    <option value="Text">Text</option>

                                </select>
                            </div>
                            <div class="form-check" options>
                                <label for="Op10">

                                    <input type="hidden"  name="Op10" value="False">
                                    <input class="form-check-input" name="Op10" type="checkbox" value="True" id="Op10">
                                </label>

                            </div>
                        </div>
                    </td>
                    <td class="Option2" data-label="Option 2">
                        <div class="OptionDetails">
                            <div class="dropdown">
                                <select class="selectpicker options" id="Option20" name="Option20" data-style="btn-success">
                                <option hidden>Op2</option>
                                    <option value="Call">Call</option>
                                    <option value="Meet">Meet</option>
                                    <option value="Text">Text</option>
                                </select>
                            </div>
                            <div class="form-check">
                                <label for="Op20">
                                    <input type="hidden" name="Op20" value="False">
                                    <input class="form-check-input" type="checkbox" name="Op20" value="True" id="Op20">
                                </label>

                            </div>

                        </div>

                    </td>
                    <td class="Option3" data-label="Option 3">
                        <div class="OptionDetails">
                            <div class="dropdown4">
                                <select class="selectpicker options" id="Option30" name="Option30" data-style="btn-success">
                                <option hidden>Op3</option>
                                    <option value="Text">Text</option>
                                    <option value="Meet">Meet</option>
                                    <option value="Call">Call</option>

                                </select>
                            </div>
                            <div class="form-check">
                                <label for="Op30">
                                    <input type="hidden" name="Op30" value="False">

                                    <input class="form-check-input" type="checkbox" name="Op30" value="True" id="Op30">
                                </label>

                            </div>
                        </div>
                    </td>

                    <td class="Status" data-label="Status">
                        <select class="selectpicker important ar" name="Status0" data-style="btn-info" id="arrow0" onchange="this.className=this.options[this.selectedIndex].className">
                            <option class="important ar">Not Started</option>
                            <option class="InProgress ar">In Progress</option>
                            <option class="InReview ar">In Review</option>
                            <option class="Completed ar">Completed</option>
                            <option class="Cancelled ar">Cancelled</option>
                        </select>

                        </div>
                    </td>


                    <td class="Deletebtn" data-label="Action">
                        <div class="ButtonWrapper1">
                            <button type="button" class="btn btn-outline-secondary AddRemove" onclick="deleteR(this)">-</button>
                        </div>
                        <div class="hide">
                            <input name="rows[]" value="0" </div>
                    </td>
                </tr>




            </tbody>
        </table>




        <hr>
        <div class="error-notice hide" id="msg">
            <div class="oaerror danger">
                <strong>Error</strong>-Please Select Date& Please try again.
            </div>
        </div>
        <div class="SaveBtn">
            <input name="Submit1" type="submit" class="btn btn-secondary" value="Save" onclick="return save_data()" />
            <div class="ButtonWrapper">
                <button type="button" name="add" class="btn btn-outline-secondary" class="AddRemove" onclick="add_text_input()">Add</button>
            </div>
            <div class="ButtonWrapper">
                <a href="view1.php"<button type="button"  class="btn btn-outline-secondary" class="AddRemove">View Tasks</button></a>
            </div>
        </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    
    <script src="test.js"></script>
    <script type="text/javascript">
        // $('tbody').sortable();
        $( "tbody" ).sortable({
    stop: function( event, ui ){
        $(this).find('tr').each(function(i){
            $(this).find('td:first-child Span').text(i+1);
            $(this).find('td:first-child input.Hidden_Pri').val(i+1);

        });
    }
});
    </script>


    <script>
        function add_text_input() {
            var table = document.getElementById("Table");
            var x = table.rows.length;
            var z = x - 1;
            var y = x;
         var value = <?php echo json_encode($data_collector); ?>;
            var value1 = <?php echo json_encode($data_collector1); ?>;
             table.insertRow(-1).innerHTML =
                '<tr class="all work">' +

                '<td class="Priority" data-label="Priority"><span class="Priority_border" id="PriorityTxt' + x + '">' + y + '</span><input type="hidden" class="Hidden_Pri" id="hidden_field_id' + x + '" name="id' + (x - 1) + '"><input type="hidden" id="Uni_Id' + x + '" name="Uni' + (x - 1) + '"></td>' +
                "<td class='Categories' data-label='Categories'><div class='dropdown'><select class='selectpicker' name='Categories" + (x - 1) + "' data-style='btn-success' id='dropdownMenuButton" + (x - 1) + "'><option hidden>Categories</option></select></div></td>" +
                '<td class="ConnetTo" data-label="Connet To"><div class="dropdown"><select class="selectpicker" name="ConnetTo' + (x - 1) + '" data-style="btn-success" id="Client' + (x - 1) + '"><option hidden>ClientProspect</option></select></div></td>' +
                '<td class="Task" data-label="Task"><div class="form-group"><textarea class="form-control" rows="1" name="note' + (x - 1) + '" id="comment' + (x - 1) + '"></textarea></div></td>' +
                '<td class="Option1" data-label="Option 1"><div class="OptionDetails"><div class="dropdown"><select class="selectpicker options"  name="Option1' + (x - 1) + '" data-style="btn-success" id="Option1' + (x - 1) + '"><option hidden>Op1</option><option value="Meet">Meet</option><option value="Call">Call</option><option value="Text">Text</option></select></div><div class="form-check" options><label for="Op1' + (x - 1) + '"><input type="hidden" name="Op1' + (x - 1) + '" value="False" ><input class="form-check-input" name="Op1' + (x - 1) + '" type="checkbox" value="True" id="Op1' + (x - 1) + '"></label></div></div></td>' +
                '<td class="Option2" data-label="Option 2"><div class="OptionDetails"><div class="dropdown"><select class="selectpicker options" name="Option2' + (x - 1) + '" data-style="btn-success" id="Option2' + (x - 1) + '"><option hidden>Op2</option><option value="Call">Call</option><option value="Meet">Meet</option><option value="Text">Text</option></select></div><div class="form-check"><label for="Op2' + (x - 1) + '"><input type="hidden" name="Op2' + (x - 1) + '" value="False" ><input class="form-check-input" type="checkbox" name="Op2' + (x - 1) + '" value="True" id="Op2' + (x - 1) + '"></label></div></div></td>' +
                '<td class="Option3" data-label="Option 3"><div class="OptionDetails"><div class="dropdown4"><select class="selectpicker options" name="Option3' + (x - 1) + '" data-style="btn-success" id="Option3' + (x - 1) + '"><option hidden>Op3</option><option value="Text">Text</option><option value="Meet">Meet</option><option value="Call">Call</option></select></div><div class="form-check"><label for="Op3' + (x - 1) + '"><input type="hidden" name="Op3' + (x - 1) + '" value="False"><input class="form-check-input" type="checkbox" name="Op3' + (x - 1) + '" value="True"id="Op3' + (x - 1) + '"></label></div></div> </td>' +
                '<td class="Status" data-label="Status"><select class="selectpicker important ar" name="Status' + (x - 1) + '" data-style="btn-info" id="arrow' + (x - 1) + '" onchange="this.className=this.options[this.selectedIndex].className"><option class="important ar">Not Started</option><option class="InProgress ar">In Progress</option><option class="InReview ar">In Review</option><option class="Completed ar">Completed</option><option class="Cancelled ar">Cancelled</option></select></div></td>' +
                '<td class="Deletebtn" data-label="Action"><div class="ButtonWrapper1"><button type="button" class="btn btn-outline-secondary AddRemove"onclick="deleteR(this)">-</button></div><div class="hide"><input name="rows[]" value=' + x +'/></div></td></tr>';
            var selectCategories = document.getElementById("dropdownMenuButton" + (x - 1));
            var selectClient = document.getElementById("Client" + (x - 1));
            for (const val of value) {
                var option = document.createElement("option");
                option.text = val;

                selectCategories.appendChild(option);

            }
            // console.log(value1);
            for (const val of value1) {
                var option1 = document.createElement("option");
                option1.text = val;

                selectClient.appendChild(option1);

            }
            var table = document.getElementById("Table");
        var y = table.rows.length;
        var xy=document.getElementById("Uni_Id1").value;
        console.log("xy",xy);

        for (m = 1; m < y; m++) {
            document.getElementById(
                "hidden_field_id" + m
            ).value = document.getElementById("PriorityTxt" + m).innerHTML;
            document.getElementById(
                "Uni_Id" + m
            ).value = document.getElementById("PriorityTxt" + m).innerHTML;

            
           
        }
        y=y-1;
        for(this.i=0;this.i<y;this.i++){
            console.log("Option1"+this.i);
            var elem= document.getElementById("Option1"+this.i);
           elem.addEventListener("click", func, false);
           function func(e){
               var a=e.target.id;
               console.log(a);
               No=a.slice(7);
               console.log(No);
           }
               document.getElementById("Option1"+this.i).onchange=function(e){
var el=this;
if(el.value==="Meet"){
    op="#Option2"+No;
    console.log(op);
    document.querySelectorAll(op+" option").forEach(option => option.remove())
    var values = ["Op2", "Call", "Text"];
    op1="Option2"+No;
    var select = document.getElementById(op1);
    for (const val of values) {
      var option = document.createElement("option");
      if (val == "Op2") {
      option.setAttribute("hidden", true);
      option.text = val;
    }
    option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }
}
        else if (el.value === "Call") {
            op="#Option2"+No;
    console.log(op);
    document.querySelectorAll(op+" option").forEach(option => option.remove())
    var values = ["Op2", "Meet", "Text"];
    op1="Option2"+No;
    console.log(op1);
    var select = document.getElementById(op1);
    for (const val of values) {
      var option = document.createElement("option");
      if (val == "Op2") {
      option.setAttribute("hidden", true);
      option.text = val;
    }
    option.text = val;  
    option.value = val;
    

      select.appendChild(option);
    }    



}else if (el.value === "Text") {

    op="#Option2"+No;
    console.log(op);
    document.querySelectorAll(op+" option").forEach(option => option.remove())
    var values = ["Op2", "Meet", "Call"];
    op1="Option2"+No;
    var select = document.getElementById(op1);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
      if (val == "Op2") {
      option.setAttribute("hidden", true);
      option.text = val;
    }
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
}
var elem1= document.getElementById("Option2"+this.i);
           elem1.addEventListener("click", func1, false);
           function func1(e){
               var b=e.target.id;
               console.log(b);
               No1=b.slice(7);
               console.log(No1);
           }


document.getElementById("Option2"+this.i).onchange=function(e){
var e1=this;
var Op33="#Option3"+No1;
var op3="Option3"+No1;
console.log(op3);
var option1select="Option1"+No1;
var source=document.getElementById(option1select).value;
if(source === "Meet" && e1.value=== "Call"){
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Text"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    
}
else if (source === "Meet" && e1.value === "Text") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Call"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    


}
else if (source === "Text" && e1.value === "Meet") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Call"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
else if (source === "Text" && e1.value === "Call") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Meet"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
else if (source === "Call" && e1.value === "Meet") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Text"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}
else if (source === "Call" && e1.value === "Text") {
    document.querySelectorAll(Op33+" option").forEach(option => option.remove())
    var values = ["Meet"];
   var select = document.getElementById(op3);
    console.log(select);
    for (const val of values) {
      var option = document.createElement("option");
    
     option.text = val;
      option.value = val;
    

      select.appendChild(option);
    }    

}

}

}
}
        function save_data() {
            var table = document.getElementById("tablebody");
            var tableRows = table.rows.length;
            // console.log(tableRows);  
            var dateValue = document.getElementById("datepicker").value;
  if (!dateValue) {
    document.getElementById("msg").classList.remove("hide");
    return false;
  } 
  else {
    var data = [];
            for (var p = 0; p <= tableRows - 1; p++) {
                // var cat = "dropdownMenuButton" + p;
                // console.log(cat);
                
                var id=document.getElementById("hidden_field_id"+(p+1)).value;
                var Categories = document.getElementById("dropdownMenuButton" + p).value;
                // console.log(Categories);
                var ConnetTo = document.getElementById("Client" + p).value;
                // console.log(ConnetTo);
                var note = document.getElementById("comment" + p).value;
                var Option1 = document.getElementById("Option1" + p).value;
                var Op1 = document.getElementById("Op1" + p).checked;
                console.log(Op1);
                var Option2 = document.getElementById("Option2" + p).value;
                var Op2 = document.getElementById("Op2" + p).checked;
                var Option3 = document.getElementById("Option3" + p).value;
                
                var Op3 = document.getElementById("Op3" + p).checked;
                var Status = document.getElementById("arrow" +p ).value;
                var temp = {
                    Id:id,
                    Categories: Categories,
                    ConnetTo: ConnetTo,
                    note: note,
                    Option1: Option1,
                    Op1: Op1,
                    Option2: Option2,
                    Op2: Op2,
                    Option3: Option3,
                    Op3: Op3,
                    Status: Status,
                };
                data.push(temp);
            }
            window.localStorage.setItem("Table1", JSON.stringify(data));
        }
        }
        

        loadData = function() {
            let data = JSON.parse(window.localStorage.getItem("Table1"));
            for (g = 0; g < data.length; g++) {
                // console.log(g);
                if (g>0) {
                    add_text_input();
                }
                // console.log(g);
                // console.log(data[0].Categories);
                document.getElementById("PriorityTxt" + (g+1)).innerHTML =
                    data[g].Id;
document.getElementById("dropdownMenuButton" + (g)).value =
                    data[g].Categories;
                document.getElementById("Client" + (g)).value = data[g].ConnetTo;
                document.getElementById("comment" + (g)).value = data[g].note;
                document.getElementById("Option1" + (g)).value = data[g].Option1;
                document.getElementById("Op1" + (g)).checked= data[g].Op1;
                document.getElementById("Option2" + (g)).value = data[g].Option2;
                document.getElementById("Op2" + (g)).checked = data[g].Op2;
                document.getElementById("Option3" + (g)).value = data[g].Option3;
                document.getElementById("Op3" + (g)).checked = data[g].Op3;
                var stat=document.getElementById("arrow" + (g));
                // console.log(stat);
                var str=data[g].Status;
                // console.log(str.replace(/\s+/g, ''));
                stat.classList.remove("important");
                stat.classList.add(str.replace(/\s+/g, ''));
                stat.value = data[g].Status;
            }
        };

        loadData();
     
    
    </script>
    <script>
      function sortTable() {
      var table, rows, switching, i, x, y, shouldSwitch;
      table = document.getElementById("Table");
      switching = true;
      /*Make a loop that will continue until
      no switching has been done:*/
      while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
          //start by saying there should be no switching:
          shouldSwitch = false;
          /*Get the two elements you want to compare,
          one from current row and one from the next:*/
          x = rows[i].getElementsByTagName("span")[0];
          console.log(x);
          y = rows[i + 1].getElementsByTagName("span")[0];
          console.log(y);
          console.log(x.innerHTML);
          console.log(y.innerHTML);
          //check if the two rows should switch place:
          if (Number(x.innerHTML) > Number(y.innerHTML)) {
            //if so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
        if (shouldSwitch) {
          /*If a switch has been marked, make the switch
          and mark that a switch has been done:*/
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true;
        }
      }
    }
    </script>
        <!-- <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script> -->

    <!-- // <script src="https://code.jquery.com/jquery.js"></script> -->
<!--  -->
        
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> -->

</body>
<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.min.css" />
<script src="js/jquery.datetimepicker.js"></script>


</html>
<?php
$connection = mysqli_connect('localhost', 'root', '') or die('Could not connect the database : Username or password incorrect');


$db_selected = mysqli_select_db($connection, 'view1');
if (!$db_selected) {
    $sql = 'CREATE DATABASE view1';
    if (mysqli_query($connection, $sql)) {
        echo "Database my_db created successfully\n";
    } else {
        echo 'Error creating database: ' . mysqli_error($connection) . "\n";
    }
}
$query = "SELECT ID FROM task";
$result = mysqli_query($connection, $query);

if (empty($result)) {
    $query = "CREATE TABLE `view1`.`task` (`Id` INT(100) NOT NULL, `Priority` INT(10) NOT NULL, `Date` DATE NOT NULL , `Categories` VARCHAR(100) NOT NULL , `ConnectTo` VARCHAR(100) NOT NULL , `Note` VARCHAR(1000) NOT NULL , `Option1` VARCHAR(100) NOT NULL , `Option2` VARCHAR(100) NOT NULL , `Option3` VARCHAR(100) NOT NULL , `Op1Status` VARCHAR(100) NOT NULL , `Op2Status` VARCHAR(100) NOT NULL , `Op3Status` VARCHAR(100) NOT NULL , `Status` VARCHAR(1000) NOT NULL , PRIMARY KEY (`Id`)) ENGINE = InnoDB;";
    $result = mysqli_query($connection, $query);
} else {
    echo 'Database Connected successfully';
}

$query_def="SELECT ID FROM definition";
$result_def= mysqli_query($connection, $query_def);
if (empty($result_def)) {
    $query = "CREATE TABLE `view1`.`task` (`Id` INT(100) AUTO_INCREMENT,`Type` VARCHAR(100) NOT NULL , `Value` VARCHAR(100) NOT NULL , PRIMARY KEY (`Id`)) ENGINE = InnoDB;";
    $result = mysqli_query($connection, $query);
} else {
    echo 'Database Connected successfully';
}




function insert_into_db($data, $connection)
{
    foreach ($data as $key => $value) {
        $k[] = $key;
        $v[] = "'" . $value . "'";
    }
    $k = implode(",", $k);
    $v = implode(",", $v);
    $query1 = "INSERT INTO `view1`.`task`($k) VALUES($v)";
    $query_run = mysqli_query($connection, $query1);
    if ($query_run) {
        echo '<script type="text/javascript">alert("Saved")</script>';
    } else {
        echo '<script type="text/javascript">alert("Not SAved")</script>';
    }
}
if (isset($_POST['Submit1'])) {
    $rows = count($_POST['rows']);
    echo count($_POST['rows']);

    for ($i = 0; $i < $rows; $i++) {
        $Pri = $_POST['id' . $i];
        $Uni=$_POST['Uni'.$i];
        echo $Uni;
        echo $Pri;


        $check = mysqli_query($connection, "select * from task where Id='$Uni'");
        // if ($check) {
        //     echo '<script type="text/javascript">alert("Successfully")</script>';
        // } else {
        //     echo '<script type="text/javascript">alert("Not SAved")</script>';
        // }
        $checkrows = mysqli_num_rows($check);
        if ($checkrows > 0) {
            echo $i;
            $cat = $_POST['Categories' . $i];
            echo $cat;
            $con = $_POST['ConnetTo' . $i];
            echo $con;
            $note = $_POST['note' . $i];
            $op1 = $_POST['Option1' . $i];
            $op2 = $_POST['Option2' . $i];
            $op3 = $_POST['Option3' . $i];
            $op1st = $_POST['Op1' . $i];
            $op2st = $_POST['Op2' . $i];
            $op3st = $_POST['Op3' . $i];
            $st = $_POST['Status' . $i];
            if (($_POST['Categories' . $i] != "Categories") && ($_POST['ConnetTo' . $i] != "ClientProspect") && ($_POST['note' . $i] != "")) {
                $qu = "UPDATE `task` SET `Priority`='$Pri',`Categories` = '$cat', `ConnectTo` = '$con', `Note` = '$note', `Option1` = '$op1', `Option2` = '$op2', `Option3` = '$op3', `Op1Status` = '$op1st', `Op2Status` = '$op2st', `Op3Status` = '$op3st', `Status` = '$st' where `task`.`Id`='$Uni'";
            }
            $check1 = mysqli_query($connection, $qu);
        }
        if (($_POST['Categories' . $i] != "Categories") && ($_POST['ConnetTo' . $i] != "ClientProspect") && ($_POST['note' . $i] != "") && ($checkrows == 0)) {
            $data = array(
                'Id' => $_POST['Uni' . $i],
                'Priority' => $_POST['id' . $i],
                'Date' => $_POST['date'],
                'Categories' => $_POST['Categories' . $i],
                'ConnectTo' => $_POST['ConnetTo' . $i],
                'Note' => $_POST['note' . $i],
                'Option1' => $_POST['Option1' . $i],
                'Option2' => $_POST['Option2' . $i],
                'Option3' => $_POST['Option3' . $i],
                'Op1Status' => $_POST['Op1' . $i],
                'Op2Status' => $_POST['Op2' . $i],
                'Op3Status' => $_POST['Op3' . $i],
                'Status' => $_POST['Status' . $i]
            );


            insert_into_db($data, $connection);
        }
    }
}

mysqli_close($connection);
?>